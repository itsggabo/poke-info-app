import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { router } from './shared/router'

import ElementPlus from 'element-plus'
import App from './App.vue'
import Dayjs from 'dayjs'
import 'element-plus/dist/index.css'
import './style.css'


const app = createApp(App)
.use(createPinia())
.use(router)
.use(ElementPlus, { size: 'medium', zIndex: 3000 })
app.config.globalProperties.$dayjs = Dayjs
app.mount('#app')

