import { Fetch } from "../../../shared/services/api"
// import type { Pokemon } from "../../../shared/types/pokemon"


export async function get_pokemon_info(){
    try {
        const response = await Fetch.get(`/pokemons`);
        return response.json();
    } catch (error) {
        console.error(error);
    }
};