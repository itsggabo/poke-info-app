import { defineStore } from "pinia";
import { get_pokemon_info } from "../services/get_pokemon_Info.service";

export default defineStore({
    id: 'pokemon',
    state: () => ({
        pokemons: [],
    }),
    actions: {
        async get_pokemons() {
            const pokemons = await get_pokemon_info(); 
            this.pokemons = pokemons;
        }
    }
})