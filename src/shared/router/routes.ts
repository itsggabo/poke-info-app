import NotFound from '../pages/not-found.vue'
import Home from '../pages/home.vue'

export const routes = [
    { path: '/:pathMatch(.*)*', name: 'not-found', component: NotFound },
    { 
        path: '/', 
        name: 'home', 
        component: Home,
    },
];

