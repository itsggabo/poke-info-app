
const baseUrl = 'https://poke-info-api.p.rapidapi.com';
const headers = {
    'X-RapidAPI-Key': '8aa0fe4dcdmsh2fa1e646c75a85ep14eae4jsn87049c56f2d0',
    'X-RapidAPI-Host': 'poke-info-api.p.rapidapi.com'
}
export class Fetch {
    static get(url: string, params: any = {}) {
        const searchParams = new URLSearchParams({ ...params });
        return fetch(baseUrl + url + searchParams, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'X-RapidAPI-Key': '8aa0fe4dcdmsh2fa1e646c75a85ep14eae4jsn87049c56f2d0',
                'X-RapidAPI-Host': 'poke-info-api.p.rapidapi.com'
            },
        });
    }
    static post(url: string, data: any) {
        return fetch(baseUrl + url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
    }
    put(url: string, data: any) {
        return fetch(baseUrl + url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
    }
    delete(url: string) {
        return fetch(baseUrl + url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
}